<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    //Table Name
    protected $Table = 'posts';
    //primary key 
    public $primaryKey = 'id';
    //timestamps
    public $timestamps = true;
}
