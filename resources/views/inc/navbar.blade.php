 <nav class="navbar navbar-inverse">
 <div class = "container">
 <div class = 'navbar-header'>

 
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
    <span class = "sr-only">toggle-navigation</span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
  </button>
  <a class="navbar-brand" href="/">{{config('app.name','MYAPP')}}</a>
  </div>

  <div class="collapse navbar-collapse" id="navbar">
    <ul class="nav navbar-nav ">
      <li>
        <a  href="/">Home </a>
      </li>
     <li>
        <a  href="/about">About  </a>
      </li>
      <li>
        <a  href="/services">Services </a>
      </li>
        <li>
        <a  href="/posts">Blog </a>
      </li>
    </ul>
    <ul class = "nav navbar-nav navbar-right">
    <li><a href="/posts/create">Create Post</a></li>
    
  </div>
</nav>