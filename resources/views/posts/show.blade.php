@extends('layouts.app');
@section('content');
<a href="/posts" class= "btn btn-default">Go back </a>
<h1>{{$post->title}}</h1>
<div>
{!!$post->body!!}
</div>
<hr>
<a href = "/posts/{{$post->id}}/edit" class= "btn btn-default">Edit</a>
<small>Created on {{$post->created_at}}</small>

@endsection;