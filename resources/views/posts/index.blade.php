@extends('layouts.app');
@section('content');
<h1>Posts</h1>
@if(count($posts)>1)
@foreach ($posts as $post )
<div class = 'well'>
<h2><a href = "/posts/{{$post->id}}">{{$post->title}} </a></h2>
<small>written on {{$post->created_at}} </small>

</div>
@endforeach

@else
<p>No posts found </p>
@endif
    

@endsection;